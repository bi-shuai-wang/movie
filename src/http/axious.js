// axios封装 响应拦截
import axios from 'axios'
// 导入stroe
import store from '@/store/index.js'
// 添加显影拦截器  请求拦截器
axios.interceptors.request.use(function(config) {
  // 设置请求头
  config.headers.authorization = store.state.token
  return config
}, function(error) {
  return Promise.reject(error)
})
axios.interceptors.response.use(function(response) {
// 操作相应数据
  const res = response.data
  // 判断xiangying响应的数据是否含有token
  if (res.token) {
    // 存到vuex中
    console.log(store)
    store.commit('changetoken', res.token)
  }
  if (res.userId) {
    // 存到vuex中
    // console.log(store)
    store.commit('setuserId', res.userId)
  }
  return res
})

export default axios

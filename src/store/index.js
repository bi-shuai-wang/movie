import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cityid: 110100,
    cityname: '北京',
    cinemaid: 1701,
    token: localStorage.getItem('token') || '',
    userId: localStorage.getItem('userid') || '',
    isshowfooter: true
  },
  mutations: {
    changeid(state, newid) {
      state.cityid = newid
    },
    changename(state, newname) {
      state.cityname = newname
    },
    changecinemaid(state, newcinemaid) {
      state.cinemaid = newcinemaid
    },
    changetoken(state, newtoken) {
      localStorage.setItem('token', newtoken)
      state.token = newtoken
    },
    // 村用户id
    setuserId(state, userid) {
      localStorage.setItem('userid', userid)
      state.userId = userid
    },
    // 修改底部显示隐藏
    show(state) {
      state.isshowfooter = true
    },
    hide(state) {
      state.isshowfooter = false
    }
  },
  actions: {
  },
  modules: {
  }
})

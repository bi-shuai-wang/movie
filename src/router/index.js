import Vue from 'vue'
import VueRouter from 'vue-router'
import movie from '@/views/movie/index.vue'
import nowplaying from '@/views/movie/nowplaying.vue'
import future from '@/views/movie/future.vue'
import moviedetail from '@/views/movie/moviedetail.vue'
import cinema from '@/views/cinema/index.vue'
import cinemadetail from '@/views/cinema/cinemadetail.vue'
import city from '@/views/city/index.vue'
import my from '@/views/my/index.vue'
import news from '@/views/news/index.vue'
import login from '@/views/my/login.vue'
Vue.use(VueRouter)

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err)
}
const routes = [
  {
    path: '/',
    component: nowplaying
  },
  {
    path: '/movie',
    component: movie,
    children: [
      {
        path: 'playing', // 嵌套的组件不需要加斜杠
        component: nowplaying
      }, {
        path: 'future',
        component: future
      }
    ]
  },
  {
    path: '/cinema',
    component: cinema

  },
  {
    path: '/news',
    component: news
  },
  {
    path: '/my',
    component: my
  },
  {
    path: '/city',
    component: city
  },
  {
    path: '/detail',
    component: moviedetail
  },

  {
    path: '/cinemadetail',
    component: cinemadetail
  }, {
    path: '/login',
    component: login
  }

]
const router = new VueRouter({
  mode: 'history', // 将路由模式，切换成了history，默认是hash - 不写
  base: process.env.BASE_URL,
  routes
})
export default router

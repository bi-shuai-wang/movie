// 封装请求的路径
const prefix = '/api'
export default {

  getcity: prefix + '/getCitiesInfo',
  getnowplay: prefix + '/getNowPlayingFilmList',
  getfuture: prefix + '/getComingSoonFilmList',
  getmoviedetail: prefix + '/getFilmInfo',
  getcinemalist: prefix + '/getCinemaList',
  getcinemainfo: prefix + '/getCinemaInfo',
  getcinemapaipian: prefix + '/getCinemaFilmsArrangement',
  getfilmchangci: prefix + '/getCinemaFilmSchedules'
}

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 将配置好的axios 文件引入
import axios from '@/http/axious.js'
// 引入iconfont图标
import '@/assets/font/iconfont.css'
import 'vant/lib/index.css'

Vue.config.productionTip = false
//  我们引入vue其实就是引入一个vue 是一个构造函数 ，给他的原型上加上方法 那些new他的对象都可以使用这个方法
Vue.prototype.$http = axios
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

const express = require('express')
const app = express()
const { createProxyMiddleware } = require('http-proxy-middleware')
app.use('/api', createProxyMiddleware({
  target: 'https://api.iynn.cn/film/api/v1',
  changeOrigin: true,
  pathRewrite: {
    '^/api': ''
  }
}))
// app.listen(3000)
app.use(express.static('dist'))
app.listen(12121)
